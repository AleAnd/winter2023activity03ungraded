public class Application{
	public static void main(String[]args){
		
		Penguin fastPenguin = new Penguin();
		
		fastPenguin.sizeOfPenguin = 4.1;    //4
		fastPenguin.swimSpeed = 9.7; //6-9
		fastPenguin.age = 8; //15-20
		
		
		Penguin fatPenguin = new Penguin();
			
		fatPenguin.sizeOfPenguin = 3.6;
		fatPenguin.swimSpeed = 5.8;
		fatPenguin.age = 12;
		
		
		Penguin[] rookery = new Penguin[3];
		
		rookery[0] = fastPenguin;
		rookery[1] = fatPenguin;
		
		
		rookery[2] = new Penguin();
		
		rookery[2].sizeOfPenguin = 3.9;
		rookery[2].swimSpeed = 7.4;
		rookery[2].age = 10;
		
		System.out.println(rookery[2].sizeOfPenguin);
		}
}