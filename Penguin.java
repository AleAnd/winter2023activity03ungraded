public class Penguin{
	public int age;
	public double swimSpeed;
	public double sizeOfPenguin;
	
	public void eat(){
		System.out.println("Welp there he goes eating again. He is " + sizeOfPenguin + " feet tall because he eats from 25 to 50 fish a day!");
	}
	public void swim(){
		System.out.println("What an impressive swimmer. Penguins have wings for swimming and typically swim from 6-9 km an hour. He swims at a solid " + swimSpeed + " kilometers per hour.");
	}
}